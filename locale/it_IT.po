#
msgid ""
msgstr "Content-Type: text/plain; charset=utf-8\n"

msgctxt "error:party.identifier:"
msgid "Party \"%(party)s\" has more than one Tax Code"
msgstr "La controparte \"%(party)s\" ha più di un Codice Fiscale"

msgctxt "error:party.identifier:"
msgid "Party \"%(party)s\" has more than one VAT number"
msgstr "La controparte \"%(party)s\" ha più di una Partita IVA"

msgctxt "error:party.identifier:"
msgid "The VAT number \"%(code)s\" on party \"%(party)s\" is not valid."
msgstr ""
"La Partita IVA \"%(code)s\" associata alla controparte \"%(party)s\" non è "
"valida."

msgctxt "error:party.identifier:"
msgid "The tax code \"%(code)s\" on party \"%(party)s\" is not valid."
msgstr ""
"Il Codice Fiscale \"%(code)s\" associato alla controparte \"%(party)s\" non "
"è valido."

msgctxt "selection:party.identifier,type:"
msgid "Italian VAT number"
msgstr "Partita IVA"

msgctxt "selection:party.identifier,type:"
msgid "Italian tax code"
msgstr "Codice Fiscale"
