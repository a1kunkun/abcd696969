import unittest


from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import suite as test_suite


class PartyItalyTestCase(ModuleTestCase):
    'Test Party Italy module'
    module = 'party_italy'


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            PartyItalyTestCase))
    return suite
