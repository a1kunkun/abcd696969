from trytond.model import ModelView, fields
from trytond.pool import PoolMeta
from stdnum.it import codicefiscale, iva

__all__ = ['PartyIdentifier']


class PartyIdentifier(metaclass=PoolMeta):
    __name__ = 'party.identifier'

    @classmethod
    def __setup__(cls):
        super(PartyIdentifier, cls).__setup__()
        cls.type.selection.append(('it_tax_code', 'Italian tax code'))
        cls.type.selection.append(('it_vat_num', 'Italian VAT number'))

        cls._error_messages.update({
                'unique_tax_code': ('Party "%(party)s" has more than one '
                                    'Tax Code'),
                'unique_vat_no': ('Party "%(party)s" has more than one '
                                  'VAT number'),
                'tax_code_invalid': ('The tax code "%(code)s" on party '
                                     '"%(party)s" is not valid.'),
                'vat_no_invalid': ('The VAT number "%(code)s" on party '
                                   '"%(party)s" is not valid.'),
                })

    def check_code(self):
        super(PartyIdentifier, self).check_code()
        partyIdentifiers = list(self.party.identifiers)
        if self.type == 'it_tax_code':
            for identifier in partyIdentifiers:
                if identifier.type == 'it_tax_code' and self != identifier:
                    self.raise_user_error('unique_tax_code', {
                            'tax_code_invalid': self.code,
                            'party': self.party.rec_name,
                            })
            if not codicefiscale.is_valid(self.code):
                # Called from pre_validate so party may not be saved yet
                if self.party and self.party.id > 0:
                    party = self.party.rec_name
                else:
                    party = ''
                self.raise_user_error('tax_code_invalid', {
                        'code': self.code,
                        'party': party,
                        })

        if self.type == 'it_vat_num':
            for identifier in partyIdentifiers:
                if identifier.type == 'it_vat_num' and self != identifier:
                    self.raise_user_error('unique_vat_no', {
                            'vat_no_invalid': self.code,
                            'party': self.party.rec_name,
                            })
            if not iva.is_valid(self.code):
                # Called from pre_validate so party may not be saved yet
                if self.party and self.party.id > 0:
                    party = self.party.rec_name
                else:
                    party = ''
                self.raise_user_error('vat_no_invalid', {
                        'code': self.code,
                        'party': party,
                        })

    @fields.depends('type', 'code')
    def on_change_with_code(self):
        code = super(PartyIdentifier, self).on_change_with_code()

        from stdnum.exceptions import ValidationError

        if self.type == 'it_tax_code':
            try:
                return codicefiscale.compact(self.code)
            except ValidationError:
                pass
        if self.type == 'it_vat_num':
            try:
                return iva.compact(self.code)
            except ValidationError:
                pass
        return code
