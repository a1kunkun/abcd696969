try:
    from trytond.modules.party_italy.tests.test_party_italy import suite
except ImportError:
    from .test_party_italy import suite

__all__ = ['suite']
