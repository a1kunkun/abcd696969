from trytond.pool import Pool
from .party import *

__all__ = ['register']


def register():
    Pool.register(
        PartyIdentifier,
        module='party_italy', type_='model')
